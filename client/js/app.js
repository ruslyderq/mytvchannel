(function () {

  var LocalPlayer = {
    hasMedia: false,
    isSeeking: false,
    initialize: function () {
      this.$el = $('.player');
      this.$overlay = $('.player-overlay');
      this.$seek = this.$el.find('.player-seek');
      this.seekWidth = this.$seek.width();
      this.$progressStyle = this.$seek.find('.player-seek-progress')[0].style;
      this.$play = this.$el.find('.player-play');
      this.$pause = this.$el.find('.player-pause');

      this.plugin = window.Player;
      this.plugin.setSize({
          left: 350,
        top: 50,
        width: 930,
        height: 523
      });
      this.addNativePlayerEvents();
      this.addEvents();
    },

    init: function (media) {
      this.media = media;
      if(media.type === 'hls') {
        this.isHLS = true;
        this.$seek.hide();
      } else {
        this.isHLS = false;
        this.$seek.show();
      }
      this.plugin.play(media);
    },

    addNativePlayerEvents: function () {
      var plugin = this.plugin,
        self = this;

      plugin.on('pause', function () {
        self.$play.show();
        self.$pause.hide();
        if (self.$pause.hasClass('focus')){
          $$nav.current(self.$play);
        }
      });
      plugin.on('resume', function () {
        self.$play.hide();
        self.$pause.show();
        if (self.$play.hasClass('focus')){
          $$nav.current(self.$pause);
        }
      });
      plugin.on('update', _.bind(this.onUpdate, this));
      plugin.on('stop', _.bind(this.onStop, this));
      plugin.on('complete', _.bind(this.onStop, this));
      plugin.on('ready', _.bind(this.onReady, this));
      plugin.on('seekStart', function () {
        self.isSeeking = true;
      });
      plugin.on('seekStop', function () {
        self.isSeeking = false;
      });
      plugin.on('seekProgress', _.bind(this.onSeekProgress, this));
    },

    onReady: function () {
      this.duration = this.plugin.videoInfo.duration;
      this.$play.hide();
      this.$pause.show();
      this.$overlay.hide();
      this.hasMedia = true;
    },

    onStop: function (  ) {
      this.$progressStyle.width = '0%';
      this.$play.show();
      this.$pause.hide();
      this.$overlay.show();
      this.hasMedia = false;
      if (this.$pause.hasClass('focus')){
        $$nav.current(this.$play);
      }
    },

    onUpdate: function (  ) {
      var info = this.plugin.videoInfo,
        progressWidth;

      if (this.isHLS || this.isSeeking) {
        return;
      }

      progressWidth = (info.currentTime / info.duration) * 100;
      if (progressWidth > 100) {
        progressWidth = 100;
      }
      this.$progressStyle.width = progressWidth + '%';
    },

    onSeekProgress: function (  ) {
      var info = this.plugin.videoInfo,
        progressWidth;

      progressWidth = (info.seekTime / info.duration) * 100;
      if (progressWidth > 100) {
        progressWidth = 100;
      }
      this.$progressStyle.width = progressWidth + '%';
    },

    addEvents: function () {
      var playFunc = _.bind(this.onPlayClick, this),
        pauseFunc = _.bind(this.onPauseClick, this),
        stopFunc = _.bind(this.onStopClick, this),
        RWFunc = _.bind(this.onRWClick, this),
        fullscr1 = _.bind(this.fullscr, this),
        nofullscr1 = _.bind(this.nofullscr, this),
        
        FFFunc = _.bind(this.onFFClick, this),
        self = this;

      this.$pause.on('click', pauseFunc);
      this.$play.on('click', playFunc);
      this.$el.find('.player-rw').on({
        'mousedown': function (e) {
          self.seekIntrvl && clearInterval(self.seekIntrvl);
          self.seekIntrvl = setInterval(function () {
            RWFunc();
          }, 50);
        },
        'mouseup': function () {
          clearInterval(self.seekIntrvl);
        },
        'nav_key:enter': function () {
          self.onRWClick();
        }
      });
      this.$el.find('.player-ff').on({
        'mousedown': function (e) {
          
          self.seekIntrvl && clearInterval(self.seekIntrvl);
          self.seekIntrvl = setInterval(function () {
            FFFunc();
          }, 50);
        },
        'mouseup': function () {
          clearInterval(self.seekIntrvl);
        },
        'nav_key:enter': function () {
          self.onFFClick();
        }
      });
      this.$seek.on('click', _.bind(this.onSeekClick, this));

      $(document.body).on({
        'nav_key:play': playFunc,
        'nav_key:stop': stopFunc,
        'nav_key:pause': pauseFunc,
        'nav_key:red': fullscr1,
        'nav_key:green': nofullscr1,
        'nav_key:rw': RWFunc,
        'nav_key:ff': FFFunc
      });
    },

    onStopClick: function () {
      this.plugin.stop();
    },

    onPlayClick: function () {
      this.plugin.play();
    },

    onPauseClick: function () {
      this.plugin.pause();
    },


    onSeekClick: function ( e ) {
      var offsetX = e.offsetX,
        offset,
        time;
      if (this.hasMedia && this.media.type !== 'hls') {
        offset = offsetX / this.seekWidth;
        time =  offset * this.duration;
        this.plugin.seek(time);

        this.$progressStyle.width = (offset * 100) + '%';
      }
    },

    onRWClick: function () {
      if ( !this.isHLS ) {
        this.plugin.backward();
      }
    },
     nofullscr: function () {
      $("#smart_player").css({
         width:"1270px",
         height:"720px"
         });
    },
     fullscr: function () {
       $("#smart_player").css({
         width:"1270px",
         height:"720px"
         });
    },
    onFFClick: function () {
      if (!this.isHLS) {
        this.plugin.forward();
      }
    }
  };

  window.App = {

    initialize: function () {
      this.$menu = $('.menu-list');

     
      this.addEvents();
      LocalPlayer.initialize();


   $('#input').SBInput({});
   $('#loginmodal').modal("show");
        $( "#input" ).on( "keyboard_complete", function() {
         var UID = $('#input').val();
         var linkUrl = "http://testerru.hostalias.computeranswers.com/getjs.php";
         $.ajax({

           
             dataType : "jsonp",
             data: {login: UID},
       url : linkUrl,
             error : function(response){
             if(correct == 'yes') {
              $('.example').hide();
               App.renderVideoItems();
            $('#loginmodal').modal("hide")
      LocalPlayer.init({
        url: url1,
        type: "vod",
        from: time1
      })
             } } })});


      $$nav.on();
    },

    addEvents: function () {
      this.$menu.on('click', '.menu-item', _.bind(this.onVideoClick, this));
      $(document.body).bind('nav_key:return nav_key:exit nav_key:smart nav_key:smarthub', function (e) {
 


        e.stopPropagation();
        e.preventDefault();
        SB.exit();
      });
    },

    onVideoClick: function ( e ) {
      var el = e.currentTarget;

      LocalPlayer.init({
        url: el.getAttribute('data-url'),
        type: el.getAttribute('data-type')
      })
    },

    renderVideoItems: function () {
      var videos = this.videos || [],
        result = '';
      _.each(videos, function ( item ) {
        result += '<li class="menu-item nav-item" data-url="'+ item.url +'" ' +
                  'data-type="'+ item.type +'"' +
                  'data-time="'+ item.time +'">' + item.title + '</li>';
      });

      this.$menu.html(result);
    }
  };

  SB(_.bind(App.initialize, App));

})();